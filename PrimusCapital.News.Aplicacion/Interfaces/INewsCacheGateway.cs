﻿using PrimusCapital.News.Data.Modelos;
using PrimusCapital.News.Data.Modelos.News.Comun.Entidades;
using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using PrimusCapital.News.Data.Modelos.RRHH.Anexos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Aplicacion.Interfaces
{
    public interface INewsCacheGateway
    {
        #region Registros Realizados
        void GuardarRegistroHistorico(HistoricoIntranet registro);
        IEnumerable<HistoricoIntranet> ObtenerHistorico();
        IEnumerable<HistoricoIntranet> ObtenerHistorico(DateTime fechaDesde, DateTime fechaHasta);
        #endregion

        #region Configuraciones
        TimeSpan ObtenerTiempoExpiracionCache();
        void GuardarNuevoTiempoExpiracionGlobal(TimeSpan nuevoDuracionCache);
        #endregion

        #region Informacion Intranet
        IEnumerable<IntranetCompletoNews> ObtenerListadoRegistrosIntranetCompleto();
        IEnumerable<IntranetCompletoNews> ObtenerListadoRegistrosIntranetPorFecha(DateTime fechaDesde, DateTime fechaHasta);
        Post ObtenerPostSegunId(int IdPost);
        IntranetCompletoNews ObtenerRegistroIntranetSegunId(int idInforme);
        void GuardarRegistroIntranetEnCache(IntranetCompletoNews registro);
        void EliminarRegistroIntranetEnCache(int Idpost);
        void GuardarRegistroEditadoIntranetEnCache(Post registro);
        void EliminarRegistroArchivosIntranetEnCache(Archivos archivo);
        #endregion  

        #region Informacion Anexos
        //IEnumerable<Anexo> ObtenerListadoRegistrosAnexostCompleto();
        #endregion
    }
}
