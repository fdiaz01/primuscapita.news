﻿using PrimusCapital.News.Data.Modelos;
using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PrimusCapital.News.Aplicacion.Interfaces
{
    public interface IAppService
    {
        #region Post
        IntranetCompletoNews GuardarPost(int idRegistroIntranet, StringBuilder comentarios, HttpFileCollectionBase file, string url, string rutaServidor, string usuario, string ip, out ErrorViewModel error);
        IntranetCompletoNewsModel ObtenerPosts();
        IntranetCompletoNewsModel.RegistrosIntranet.PostModel ObtenerPostSegunId(int IdPost);
        UsuarioAD UsuarioValido(string username, string password, out ErrorViewModel error);
        void EliminarPost(int idPost, string usuario, string ip, out ErrorViewModel error);
        IntranetCompletoNews EditarPost(int IdPost, List<int> IdArchivos, StringBuilder comentarios, HttpFileCollectionBase file, string url, string rutaServidor, string usuario, string ip, out ErrorViewModel error);
        #endregion

        #region Informacion Anexos
        //IntranetCompletoNewsModel ObtenerAnexos();
        List<IntranetCompletoNewsModel.AnexosModel> ObtenerFotosAnexos(List<IntranetCompletoNewsModel.AnexosModel> anexos);
        #endregion
    }
}
