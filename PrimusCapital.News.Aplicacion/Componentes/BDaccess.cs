﻿using PrimusCapital.News.Data.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace PrimusCapital.News.Aplicacion.Componentes
{
    public class BDaccess
    {
        static string Ambiente = WebConfigurationManager.AppSettings["TipoAmbiente"].ToString();
        PetaPoco.Database db = new PetaPoco.Database(WebConfigurationManager.AppSettings[Ambiente + "PetapocoBDRRHH"].ToString());

        private List<SpAnexos> SpReporteDeudasDetalle(int vigente)
        {
            var respuesta = new List<SpAnexos>();
            respuesta = db.Fetch<SpAnexos>(";exec BD_RR_HH.dbo.SpAnexos @vigente", new { vigente = vigente });
            return respuesta;
        }

        public DateTime FechaNacimiento(string Supuestafecha)
        {
            DateTime fechaRetorno = new DateTime();
            try
            {
                if (!string.IsNullOrEmpty(Supuestafecha))
                {
                    string[] words = Supuestafecha.Split(' ');
                    if (words.Count() == 3)
                    {
                        switch (words[2])
                        {
                            case "Enero":
                                fechaRetorno = new DateTime(1975, 1, int.Parse(words[0]));
                                break;
                            case "Febrero":
                                fechaRetorno = new DateTime(1975, 2, int.Parse(words[0]));
                                break;
                            case "Marzo":
                                fechaRetorno = new DateTime(1975, 3, int.Parse(words[0]));
                                break;
                            case "Abril":
                                fechaRetorno = new DateTime(1975, 4, int.Parse(words[0]));
                                break;
                            case "Mayo":
                                fechaRetorno = new DateTime(1975, 5, int.Parse(words[0]));
                                break;
                            case "Junio":
                                fechaRetorno = new DateTime(1975, 6, int.Parse(words[0]));
                                break;
                            case "Julio":
                                fechaRetorno = new DateTime(1975, 7, int.Parse(words[0]));
                                break;
                            case "Agosto":
                                fechaRetorno = new DateTime(1975, 8, int.Parse(words[0]));
                                break;
                            case "Septiembre":
                                fechaRetorno = new DateTime(1975, 9, int.Parse(words[0]));
                                break;
                            case "Octubre":
                                fechaRetorno = new DateTime(1975, 10, int.Parse(words[0]));
                                break;
                            case "Noviembre":
                                fechaRetorno = new DateTime(1975, 11, int.Parse(words[0]));
                                break;
                            case "Diciembre":
                                fechaRetorno = new DateTime(1975, 12, int.Parse(words[0]));
                                break;
                            default:
                                fechaRetorno = new DateTime();
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                GestorLog.GeneraLog(e);
            }

            return fechaRetorno;
        }

        public List<IntranetCompletoNewsModel.AnexosModel> ListaAnexos()
        {
            List<IntranetCompletoNewsModel.AnexosModel> listaAnexos = null;

            //Lammada a los procedimientos
            List<SpAnexos> SpAnexos = new List<SpAnexos>();
            SpAnexos = SpReporteDeudasDetalle(1);

            //anexos
            listaAnexos = new List<IntranetCompletoNewsModel.AnexosModel>();
            IntranetCompletoNewsModel.AnexosModel Anexo = null;
            if (SpAnexos != null)
            {
                foreach (var data in SpAnexos)
                {
                    Anexo = new IntranetCompletoNewsModel.AnexosModel();
                    Anexo.Apellido_P = data.Apellido_P;
                    Anexo.Apellido_M = data.Apellido_M;
                    Anexo.nombre = data.nombre;
                    Anexo.fecha_n = data.fecha_n;
                    Anexo.area = data.area;
                    Anexo.anexo = data.anexo;
                    Anexo.mail = data.mail;
                    Anexo.foto = data.foto;
                    Anexo.cargo = data.cargo;
                    Anexo.userid = data.userid;
                    Anexo.IP = data.IP;
                    Anexo.Rut = data.Rut;
                    Anexo.DV = data.DV;
                    Anexo.correosuperior = data.correosuperior;
                    Anexo.fec_fin_contrato = data.fec_fin_contrato;
                    Anexo.fec_ini_contrato = data.fec_ini_contrato;
                    Anexo.vigente = data.vigente;
                    Anexo.sucursal = data.sucursal;
                    Anexo.fecha_nacimiento = FechaNacimiento(data.fecha_n);

                    listaAnexos.Add(Anexo);
                }
                Anexo = null;
            }

            return listaAnexos;
        }
    }
}
