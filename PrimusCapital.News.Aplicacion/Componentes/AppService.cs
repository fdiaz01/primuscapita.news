﻿using PrimusCapital.News.Aplicacion.Interfaces;
using PrimusCapital.News.Data.Modelos;
using PrimusCapital.News.Data.Modelos.News.Comun.Entidades;
using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using PrimusCapital.News.Data.Modelos.RRHH.Anexos.Entidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PrimusCapital.News.Aplicacion.Componentes
{
    [Serializable]
    public class AppService : IAppService
    {
        private readonly INewsCacheGateway _queryService;
        private readonly IntranetCompletoNewsMapper _IntranetCompletoNewsMapperMapper;

        public AppService(INewsCacheGateway queryService, IntranetCompletoNewsMapper intranetCompletoNewsMapperMapper)
        {
            _queryService = queryService;
            _IntranetCompletoNewsMapperMapper = intranetCompletoNewsMapperMapper;
        }

        #region Post
        public IntranetCompletoNews GuardarPost(int idRegistroIntranet, StringBuilder comentarios, HttpFileCollectionBase files, string url, string rutaServidor, string usuario, string ip, out ErrorViewModel error)
        {
            IntranetCompletoNews entidad = null;
            error = new ErrorViewModel();

            if (comentarios.Length <= 0 && files.Count <= 0 /*&& string.IsNullOrEmpty(url)*/)
            {
                error.CodigoError = -1;
                error.DescripcionError = "Error al Guardar el Post, faltan datos de entrada";
                return entidad;
            }

            entidad = new IntranetCompletoNews();
            List<Post> listaPost = new List<Post>();

            Post datoPost = new Post();
            datoPost.Comentarios = comentarios.ToString();

            List<Archivos> listaArchivos = new List<Archivos>();
            Archivos datoArchivo = null;
            for (int i = 0; i < files.Count; i++)
            {
                Guid id = Guid.NewGuid();
                var file = files[i];

                if (file != null && file.ContentLength > 0)
                {
                    string nombreArchivo = id.ToString() + Path.GetExtension(file.FileName);
                    string rutaCompleta = Path.Combine(rutaServidor, nombreArchivo);
                    file.SaveAs(rutaCompleta);

                    datoArchivo = new Archivos();
                    datoArchivo.NombreArchivo = nombreArchivo;
                    datoArchivo.NombreMostrar = file.FileName;
                    datoArchivo.Ruta = rutaCompleta;
                    datoArchivo.TipoArchivo = file.ContentType.ToString();
                    datoArchivo.Fecha = DateTime.Now;
                    datoArchivo.Descripccion = string.Empty;
                    if (i == 1)
                        datoArchivo.ArchivoPortada = true;
                    else
                        datoArchivo.ArchivoPortada = false;

                    listaArchivos.Add(datoArchivo);
                }
            }
            datoPost.FotosRelacionadas = listaArchivos;
            datoPost.Url = url;
            datoPost.Fecha = DateTime.Now;
            listaPost.Add(datoPost);

            HistoricoIntranet datoHistorico = new HistoricoIntranet();
            datoHistorico.Fecha = DateTime.Now;
            datoHistorico.CargadoDesdeCache = false;
            datoHistorico.UusuarioPost = usuario;
            datoHistorico.Ip = ip;
            _queryService.GuardarRegistroHistorico(datoHistorico);

            entidad.Post = listaPost;
            entidad.Fecha = DateTime.Now;
            _queryService.GuardarRegistroIntranetEnCache(entidad);

            return entidad;
        }

        public IntranetCompletoNewsModel ObtenerPosts()
        {
            IntranetCompletoNewsModel model = new IntranetCompletoNewsModel();
            IEnumerable<IntranetCompletoNews> entidad = new List<IntranetCompletoNews>();
            entidad = _queryService.ObtenerListadoRegistrosIntranetCompleto();
            model = _IntranetCompletoNewsMapperMapper.MapIntranetEntity(entidad);
            return model;
        }

        public IntranetCompletoNewsModel.RegistrosIntranet.PostModel ObtenerPostSegunId(int IdPost)
        {
            IntranetCompletoNewsModel.RegistrosIntranet.PostModel model = new IntranetCompletoNewsModel.RegistrosIntranet.PostModel();
            Post entidad = new Post();
            entidad = _queryService.ObtenerPostSegunId(IdPost);
            model = _IntranetCompletoNewsMapperMapper.MapPostEntity(entidad);
            return model;
        }

        public void EliminarPost(int idPost, string usuario, string ip, out ErrorViewModel error)
        {
            error = new ErrorViewModel();
            //IntranetCompletoNewsModel model = new IntranetCompletoNewsModel();

            HistoricoIntranet datoHistorico = new HistoricoIntranet();
            datoHistorico.Fecha = DateTime.Now;
            datoHistorico.CargadoDesdeCache = false;
            datoHistorico.UusuarioPost = usuario;
            datoHistorico.Ip = ip;
            _queryService.GuardarRegistroHistorico(datoHistorico);

            _queryService.EliminarRegistroIntranetEnCache(idPost);
        }

        public IntranetCompletoNews EditarPost(int idPost, List<int> IdsArchivos, StringBuilder comentarios, HttpFileCollectionBase files, string url, string rutaServidor, string usuario, string ip, out ErrorViewModel error)
        {
            IntranetCompletoNews entidad = null;
            error = new ErrorViewModel();

            int contador = 0;
            if (comentarios.Length <= 0 && files.Count <= 0)
            {
                error.CodigoError = -1;
                error.DescripcionError = "Error al editar el Post, faltan datos de entrada";
                return entidad;
            }

            //validacion de fotos
            Post DatoPostAnterior = new Post();
            DatoPostAnterior = _queryService.ObtenerPostSegunId(idPost);

            if (DatoPostAnterior == null)
            {
                error.CodigoError = -1;
                error.DescripcionError = "Error al editar el Post, al obtener el Post";
                return entidad;
            }

            bool ArchivoPortadaEliminado = false;
            IEnumerable<Archivos> dataArchivos = new List<Archivos>();
            IEnumerable<Archivos> dataArchivos2 = new List<Archivos>();
            if (IdsArchivos.Count > 0)
            {
                dataArchivos = DatoPostAnterior.FotosRelacionadas.Where(x => IdsArchivos.Contains(x.Id));
                dataArchivos2 = DatoPostAnterior.FotosRelacionadas.Where(x => IdsArchivos.Contains(x.Id) && x.ArchivoPortada == true);
                if (dataArchivos2.Count() > 0)
                    ArchivoPortadaEliminado = true;

                List<Archivos> listaBorrar = new List<Archivos>();
                if (dataArchivos != null)
                {
                    if (DatoPostAnterior.FotosRelacionadas.Count() == IdsArchivos.Count())
                    {
                        error.CodigoError = -1;
                        error.DescripcionError = "No se pueden eliminar toda las Imagenes del Post";
                        return entidad;
                    }

                    for (int i = 0; i < files.Count; i++)
                    {
                        var file = files[i];
                        if (file != null && file.ContentLength > 0)
                            contador++;
                    }

                    foreach (var data in dataArchivos)
                    {
                        if (File.Exists(data.Ruta))
                            File.Delete(data.Ruta);

                        var registroEliminar = DatoPostAnterior.FotosRelacionadas.FirstOrDefault(x => x.Id == data.Id);
                        listaBorrar.Add(registroEliminar);
                        //DatoPostAnterior.FotosRelacionadas.Remove(registroEliminar);
                    }

                    foreach (var data in listaBorrar)
                    {
                        _queryService.EliminarRegistroArchivosIntranetEnCache(data);
                        DatoPostAnterior.FotosRelacionadas.Remove(data);
                    }
                }
            }
            dataArchivos2 = null;

            //cosas para guardar en el post editado
            entidad = new IntranetCompletoNews();
            List<Post> listaPost = new List<Post>();

            //Post datoPost = new Post();
            DatoPostAnterior.Comentarios = comentarios.ToString();

            dataArchivos = new List<Archivos>();
            List<Archivos> listaArchivos = new List<Archivos>();
            Archivos datoArchivo = null;

            if (dataArchivos.Count() <= 0)
            {
                dataArchivos = DatoPostAnterior.FotosRelacionadas;
                foreach (var data in dataArchivos)
                {
                    if (contador == 0 && ArchivoPortadaEliminado == true)
                    {
                        data.ArchivoPortada = true;
                        ArchivoPortadaEliminado = false;
                    }

                    listaArchivos.Add(data);
                }
                dataArchivos = null;
            }
            else
            {
                foreach (var data in dataArchivos)
                {
                    if (contador == 0 && ArchivoPortadaEliminado == true)
                    {
                        data.ArchivoPortada = true;
                        ArchivoPortadaEliminado = false;
                    }

                    listaArchivos.Add(data);
                }
                dataArchivos = null;
            }

            for (int i = 0; i < files.Count; i++)
            {
                Guid id = Guid.NewGuid();
                var file = files[i];

                if (file != null && file.ContentLength > 0)
                {
                    string nombreArchivo = id.ToString() + Path.GetExtension(file.FileName);
                    string rutaCompleta = Path.Combine(rutaServidor, nombreArchivo);
                    file.SaveAs(rutaCompleta);

                    datoArchivo = new Archivos();
                    datoArchivo.NombreArchivo = nombreArchivo;
                    datoArchivo.NombreMostrar = file.FileName;
                    datoArchivo.Ruta = rutaCompleta;
                    datoArchivo.TipoArchivo = file.ContentType.ToString();
                    datoArchivo.Fecha = DateTime.Now;
                    datoArchivo.Descripccion = string.Empty;
                    if (i == 1 && ArchivoPortadaEliminado == true)
                        datoArchivo.ArchivoPortada = true;
                    else
                        datoArchivo.ArchivoPortada = false;

                    listaArchivos.Add(datoArchivo);
                }
            }
            DatoPostAnterior.FotosRelacionadas = listaArchivos;
            DatoPostAnterior.Url = url;
            //DatoPostAnterior.Fecha = DateTime.Now;
            listaPost.Add(DatoPostAnterior);

            /*HistoricoIntranet datoHistorico = new HistoricoIntranet();
            datoHistorico.Fecha = DateTime.Now;
            datoHistorico.CargadoDesdeCache = false;
            datoHistorico.UusuarioPost = usuario;
            datoHistorico.Ip = ip;
            _queryService.GuardarRegistroHistorico(datoHistorico);*/

            _queryService.GuardarRegistroEditadoIntranetEnCache(DatoPostAnterior);

            return entidad;
        }
        #endregion

        #region Login
        public UsuarioAD UsuarioValido(string username, string password, out ErrorViewModel error)
        {
            UsuarioAD usuarioConDetalles = new UsuarioAD();
            error = new ErrorViewModel();

            ActiveDirectoryService activeDirectory = new ActiveDirectoryService();
            usuarioConDetalles = activeDirectory.UsuarioValido(username, password, out error);

            return usuarioConDetalles;
        }
        #endregion

        #region Anexos
        /*public IntranetCompletoNewsModel ObtenerAnexos()
        {
            IntranetCompletoNewsModel model = new IntranetCompletoNewsModel();
            IEnumerable<Anexo> entidad = new List<Anexo>();
            entidad = _queryService.ObtenerListadoRegistrosAnexostCompleto();
            //model = _IntranetCompletoNewsMapperMapper.MapIntranetEntity(entidad);
            return model;
        }*/

        public List<IntranetCompletoNewsModel.AnexosModel> ObtenerFotosAnexos(List<IntranetCompletoNewsModel.AnexosModel> anexos)
        {
            List<IntranetCompletoNewsModel.AnexosModel> retorno = new List<IntranetCompletoNewsModel.AnexosModel>();
            ActiveDirectoryService activeDirectory = new ActiveDirectoryService();
            retorno = activeDirectory.ObtenerFotosAnexos(anexos);

            return anexos;
        }
        #endregion
    }
}
