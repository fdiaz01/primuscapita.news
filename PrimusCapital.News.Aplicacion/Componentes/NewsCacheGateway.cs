﻿using PrimusCapital.News.Data.Migrations;
using PrimusCapital.News.Data.Modelos.News.Comun.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using PrimusCapital.News.Aplicacion.Interfaces;
using PrimusCapital.News.Data.Modelos.RRHH.Anexos.Entidades;
using System.IO;

namespace PrimusCapital.News.Aplicacion.Componentes
{
    public class NewsCacheGateway : INewsCacheGateway
    {
        private readonly PrimusCacheNewsDbContext _context;
        private readonly PrimusCacheRRHHDbContext _contextRRHH;

        public NewsCacheGateway(PrimusCacheNewsDbContext context, PrimusCacheRRHHDbContext contextRRHH)
        {
            _context = context;
            _contextRRHH = contextRRHH;
        }

        #region Registros Realizados
        public void GuardarRegistroHistorico(HistoricoIntranet registro)
        {
            _context.RegistroHistorico.Add(registro);
            _context.SaveChanges();
        }

        public IEnumerable<HistoricoIntranet> ObtenerHistorico()
        {
            var registro = _context.RegistroHistorico.Include(x => x.IntranetCompletoNewsAsociado).ToList();
            return registro;
        }

        public IEnumerable<HistoricoIntranet> ObtenerHistorico(DateTime fechaDesde, DateTime fechaHasta)
        {
            var registro = _context.RegistroHistorico.Where(x => x.Fecha >= fechaDesde && x.Fecha <= fechaHasta).ToList();
            return registro;
        }
        #endregion

        #region Configuraciones
        public TimeSpan ObtenerTiempoExpiracionCache()
        {
            return _context.ConfiguracionNews.First().DuracionCacheGlobal;
        }

        public void GuardarNuevoTiempoExpiracionGlobal(TimeSpan nuevoDuracionCache)
        {
            var config = _context.ConfiguracionNews.First();
            config.DuracionCacheGlobal = nuevoDuracionCache;
            _context.SaveChanges();
        }
        #endregion  

        #region Informacion Intranet
        public IEnumerable<IntranetCompletoNews> ObtenerListadoRegistrosIntranetCompleto()
        {
            IEnumerable<IntranetCompletoNews> listaRetorno = new List<IntranetCompletoNews>();
            listaRetorno = _context.RegistroIntranet
                .Include(x => x.Post)
                .Include(x => x.Post.Select(y => y.FotosRelacionadas))
                .OrderByDescending(x => x.Fecha);

            return listaRetorno;
        }

        public IEnumerable<IntranetCompletoNews> ObtenerListadoRegistrosIntranetPorFecha(DateTime fechaDesde, DateTime fechaHasta)
        {
            IEnumerable<IntranetCompletoNews> listaRetorno = new List<IntranetCompletoNews>();
            listaRetorno = _context.RegistroIntranet
                .Include(x => x.Post)
                .OrderByDescending(x => x.Fecha)
                .Where(x => x.Fecha >= fechaDesde && x.Fecha <= fechaHasta);

            return listaRetorno;
        }

        public Post ObtenerPostSegunId(int IdPost)
        {
            Post post = _context.Post
                .Include(x => x.FotosRelacionadas)
                .FirstOrDefault(x => x.Id == IdPost);

            return post;
        }

        public IntranetCompletoNews ObtenerRegistroIntranetSegunId(int idInforme)
        {
            IntranetCompletoNews registroRetorno = _context.RegistroIntranet
                .Include(x => x.Post)
                .FirstOrDefault(x => x.Id == idInforme);

            return registroRetorno;
        }

        public void GuardarRegistroIntranetEnCache(IntranetCompletoNews registro)
        {
            if (registro == null) throw new ArgumentNullException("RegistroNews");
            _context.RegistroIntranet.Add(registro);
            _context.SaveChanges();
        }

        public void EliminarRegistroIntranetEnCache(int IdPost)
        {
            //var registro = _context.Post.FirstOrDefault(p => p.Id == Idpost);
            var registro = _context.Post
                .Include(x => x.FotosRelacionadas)
                .FirstOrDefault(x => x.Id == IdPost);

            if (registro.FotosRelacionadas != null){
                foreach(var data in registro.FotosRelacionadas){
                    if (File.Exists(data.Ruta)){
                        File.Delete(data.Ruta);
                    }
                }
            }
            _context.Post.Remove(registro);
            _context.SaveChanges();
        }

        public void EliminarRegistroArchivosIntranetEnCache(Archivos archivo)
        {
            _context.Archivos.Remove(archivo);
            _context.SaveChanges();
        }

        public void GuardarRegistroEditadoIntranetEnCache(Post registro)
        {
            if (registro == null) throw new ArgumentNullException("RegistroNews");
            _context.Post.Attach(registro);
            _context.Entry(registro).State = EntityState.Modified;
            _context.SaveChanges();
        }
        #endregion  

        #region Informacion Anexos
        /*public IEnumerable<Anexo> ObtenerListadoRegistrosAnexostCompleto()
        {
            IEnumerable<Anexo> listaRetorno = new List<Anexo>();
            listaRetorno = _contextRRHH.RegistroAnexo
                //.Include(x => x.Post)
                //.Include(x => x.Post.Select(y => y.FotosRelacionadas))
                .OrderByDescending(x => x.id);

            return listaRetorno;
        }*/
        #endregion
    }
}
