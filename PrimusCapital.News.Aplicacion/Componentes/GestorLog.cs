﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Aplicacion.Componentes
{
    [Serializable]
    public class GestorLog
    {

        public static void GeneraLog(Exception ex)
        {
            try
            {
                string Ambiente = ConfigurationManager.AppSettings["TipoAmbiente"].ToString();
                switch (ConfigurationManager.AppSettings[Ambiente + "TipoLog"].Trim())
                {
                    case "ExceptionHandler":
                        Logger.Write(ex);
                        break;
                    case "EventLog":
                        string Source = string.Empty, Log = string.Empty, Event = string.Empty;

                        Source = "BSEC_SIR";
                        Log = "Application";
                        Event = ex.ToString().Trim();

                        if (!EventLog.SourceExists(Source))
                            EventLog.CreateEventSource(Source, Log);

                        EventLog.WriteEntry(Source, Event, EventLogEntryType.Error);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                //esta exepcion es por si no funciona el log
            }
        }

        public static void GeneraLog(Exception ex, StringBuilder commentarios)
        {
            try
            {
                string Ambiente = ConfigurationManager.AppSettings["TipoAmbiente"].ToString();
                switch (ConfigurationManager.AppSettings[Ambiente + "TipoLog"].Trim())
                {
                    case "ExceptionHandler":
                        Logger.Write(ex);

                        if (commentarios != null)
                            Logger.Write(commentarios);
                        break;
                    case "EventLog":
                        string Source = string.Empty, Log = string.Empty, Event = string.Empty;

                        Source = "BSEC_SIR";
                        Log = "Application";
                        Event = ex.ToString().Trim();

                        if (!EventLog.SourceExists(Source))
                            EventLog.CreateEventSource(Source, Log);

                        EventLog.WriteEntry(Source, Event, EventLogEntryType.Error);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                //esta exepcion es por si no funciona el log
            }
        }

        public static void GeneraLog(Exception ex, string commentarios)
        {
            try
            {
                string Ambiente = ConfigurationManager.AppSettings["TipoAmbiente"].ToString();
                switch (ConfigurationManager.AppSettings[Ambiente + "TipoLog"].Trim())
                {
                    case "ExceptionHandler":
                        Logger.Write(ex);

                        if (commentarios != null)
                            Logger.Write(commentarios);
                        break;
                    case "EventLog":
                        string Source = string.Empty, Log = string.Empty, Event = string.Empty;

                        Source = "BSEC_SIR";
                        Log = "Application";
                        Event = ex.ToString().Trim();

                        if (!EventLog.SourceExists(Source))
                            EventLog.CreateEventSource(Source, Log);

                        EventLog.WriteEntry(Source, Event, EventLogEntryType.Error);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                //esta exepcion es por si no funciona el log
            }
        }
    }
}
