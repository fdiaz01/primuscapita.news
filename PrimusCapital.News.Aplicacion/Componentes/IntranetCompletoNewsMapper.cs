﻿using PrimusCapital.News.Data.Migrations;
using PrimusCapital.News.Data.Modelos;
using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Aplicacion.Componentes
{
    [Serializable]
    public class IntranetCompletoNewsMapper
    {
        private readonly PrimusCacheNewsDbContext _context;

        public IntranetCompletoNewsMapper(PrimusCacheNewsDbContext context)
        {
            _context = context;
        }

        public IntranetCompletoNewsModel MapIntranetEntity(IEnumerable<IntranetCompletoNews> intranet)
        {
            var model = new IntranetCompletoNewsModel();

            if (intranet != null)
            {
                List<IntranetCompletoNewsModel.RegistrosIntranet> ListaIntranet = new List<IntranetCompletoNewsModel.RegistrosIntranet>();
                IntranetCompletoNewsModel.RegistrosIntranet Intranet = null;
                foreach (var datos in intranet)
                {
                    Intranet = new IntranetCompletoNewsModel.RegistrosIntranet();
                    Intranet.Fecha = datos.Fecha;
                    Intranet.Id = datos.Id;

                    if (datos.Post != null)
                    {
                        List<IntranetCompletoNewsModel.RegistrosIntranet.PostModel> ListaPost = new List<IntranetCompletoNewsModel.RegistrosIntranet.PostModel>();
                        IntranetCompletoNewsModel.RegistrosIntranet.PostModel Post = null;
                        foreach (var dato0 in datos.Post)
                        {
                            Post = new IntranetCompletoNewsModel.RegistrosIntranet.PostModel();
                            Post.Fecha = dato0.Fecha;
                            Post.Id = dato0.Id;
                            Post.Comentarios = dato0.Comentarios;
                            Post.Url = dato0.Url;

                            if (dato0.FotosRelacionadas != null)
                            {
                                List<IntranetCompletoNewsModel.RegistrosIntranet.ArchivosModel> ListaArchivo = new List<IntranetCompletoNewsModel.RegistrosIntranet.ArchivosModel>();
                                IntranetCompletoNewsModel.RegistrosIntranet.ArchivosModel Archivo = null;
                                foreach (var dato1 in dato0.FotosRelacionadas)
                                {
                                    Archivo = new IntranetCompletoNewsModel.RegistrosIntranet.ArchivosModel();
                                    Archivo.ArchivoPortada = dato1.ArchivoPortada;
                                    Archivo.Descripccion = dato1.Descripccion;
                                    Archivo.Fecha = dato1.Fecha;
                                    Archivo.Id = dato1.Id;
                                    Archivo.NombreArchivo = dato1.NombreArchivo;
                                    Archivo.NombreMostrar = dato1.NombreMostrar;
                                    Archivo.Ruta = dato1.Ruta;
                                    Archivo.TipoArchivo = dato1.TipoArchivo;
                                    ListaArchivo.Add(Archivo);
                                }
                                Post.FotosRelacionadas = ListaArchivo;
                                Archivo = null;
                                ListaArchivo = null;
                            }
                            ListaPost.Add(Post);
                        }
                        Intranet.Posts = ListaPost;
                        Post = null;
                        ListaPost = null;
                    }
                    ListaIntranet.Add(Intranet);
                }
                model.Intranet = ListaIntranet;
                Intranet = null;
                ListaIntranet = null;
            }

            return model;
        }

        public IntranetCompletoNewsModel.RegistrosIntranet.PostModel MapPostEntity(Post post)
        {
            var model = new IntranetCompletoNewsModel.RegistrosIntranet.PostModel();

            if (post != null)
            {
                model = new IntranetCompletoNewsModel.RegistrosIntranet.PostModel();
                model.Fecha = post.Fecha;
                model.Id = post.Id;
                model.Comentarios = post.Comentarios;
                model.Url = post.Url;

                if (post.FotosRelacionadas != null)
                {
                    List<IntranetCompletoNewsModel.RegistrosIntranet.ArchivosModel> ListaArchivo = new List<IntranetCompletoNewsModel.RegistrosIntranet.ArchivosModel>();
                    IntranetCompletoNewsModel.RegistrosIntranet.ArchivosModel Archivo = null;
                    foreach (var dato1 in post.FotosRelacionadas)
                    {
                        Archivo = new IntranetCompletoNewsModel.RegistrosIntranet.ArchivosModel();
                        Archivo.ArchivoPortada = dato1.ArchivoPortada;
                        Archivo.Descripccion = dato1.Descripccion;
                        Archivo.Fecha = dato1.Fecha;
                        Archivo.Id = dato1.Id;
                        Archivo.NombreArchivo = dato1.NombreArchivo;
                        Archivo.NombreMostrar = dato1.NombreMostrar;
                        Archivo.Ruta = dato1.Ruta;
                        Archivo.TipoArchivo = dato1.TipoArchivo;
                        ListaArchivo.Add(Archivo);
                    }
                    model.FotosRelacionadas = ListaArchivo;
                    Archivo = null;
                    ListaArchivo = null;
                }
            }

            return model;
        }

    }
}
