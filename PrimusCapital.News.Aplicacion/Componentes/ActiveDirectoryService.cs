﻿using PrimusCapital.News.Data.Modelos;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace PrimusCapital.News.Aplicacion.Componentes
{
    public class ActiveDirectoryService
    {
        public UsuarioAD UsuarioValido(string username, string password, out ErrorViewModel error)
        {
            UsuarioAD usuarioConDetalles = null;
            error = new ErrorViewModel();

            string Ambiente = WebConfigurationManager.AppSettings["TipoAmbiente"].ToString();
            string Dominio = WebConfigurationManager.AppSettings[Ambiente + "AdDomain"].ToString();

            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, Dominio, username, password);

            if (ctx.ValidateCredentials(username, password))
            {
                usuarioConDetalles = new UsuarioAD();
                usuarioConDetalles = ObtenerDetallesUsuario(ctx, username, password);
                usuarioConDetalles.Logueado = true;

                string[] usuariosAdmin = WebConfigurationManager.AppSettings[Ambiente + "UsuariosAdmin"].Split(';');

                if (usuarioConDetalles != null)
                {
                    if (usuariosAdmin.Contains(usuarioConDetalles.Username))
                    {
                        usuarioConDetalles.Permitido = true;
                        usuarioConDetalles.Roles.Add("ADMIN");
                    }
                }
                else
                {
                    usuarioConDetalles.Permitido = false;
                }
                return usuarioConDetalles;
            }
            else
            {
                error.CodigoError = -1;
                error.DescripcionError = "Usuario o contraseña Incorrectos!";
            }

            return usuarioConDetalles;
        }

        private UsuarioAD ObtenerDetallesUsuario(PrincipalContext ctx, string userName, string password)
        {
            List<UserPrincipal> searchPrinciples = new List<UserPrincipal>();
            searchPrinciples.Add(new UserPrincipal(ctx) { DisplayName = String.Format("*{0}*", userName) });
            searchPrinciples.Add(new UserPrincipal(ctx) { SamAccountName = String.Format("*{0}*", userName) });
            searchPrinciples.Add(new UserPrincipal(ctx) { Surname = String.Format("*{0}*", userName) });
            searchPrinciples.Add(new UserPrincipal(ctx) { GivenName = String.Format("*{0}*", userName) });

            List<Principal> results = new List<Principal>();
            var searcher = new PrincipalSearcher();

            IList<UsuarioAD> userList = new List<UsuarioAD>();

            foreach (var item in searchPrinciples)
            {
                searcher = new PrincipalSearcher(item);
                foreach (Principal found in searcher.FindAll())
                {
                    DirectoryEntry directoryEntry = (DirectoryEntry)found.GetUnderlyingObject();
                    PropertyValueCollection fotoAD = directoryEntry.Properties["thumbnailPhoto"];
                    byte[] fotoEnBytes = new byte[0];

                    if (fotoAD.Value != null && fotoAD.Value is byte[])
                    {
                        fotoEnBytes = (byte[])fotoAD.Value;
                    }

                    UsuarioAD usuarioActiveDirectory = new UsuarioAD();

                    var gruposEsteUsuario = found.GetGroups().ToList();

                    UserPrincipal user = ((UserPrincipal)found);
                    usuarioActiveDirectory.Username = user.SamAccountName;
                    usuarioActiveDirectory.Nombre = user.GivenName;
                    usuarioActiveDirectory.Apellido = user.Surname;
                    usuarioActiveDirectory.FotoBytes = fotoEnBytes;
                    usuarioActiveDirectory.FotoUrl = (fotoEnBytes.Length > 1) ? String.Format("data:image/jpeg;base64,{0}", Convert.ToBase64String(fotoEnBytes)) : null;

                    userList.Add(usuarioActiveDirectory);
                    break;
                }
            }
            return userList.First();
        }

        public List<IntranetCompletoNewsModel.AnexosModel> ObtenerFotosAnexos(List<IntranetCompletoNewsModel.AnexosModel> anexos)
        {
            string Ambiente = WebConfigurationManager.AppSettings["TipoAmbiente"].ToString();
            string Dominio = WebConfigurationManager.AppSettings[Ambiente + "AdDomain"].ToString();

            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, Dominio);

            if (anexos != null)
            {
                foreach (var data in anexos)
                {
                    if (!string.IsNullOrEmpty(data.userid))
                    {
                        List<UserPrincipal> searchPrinciples = new List<UserPrincipal>();
                        searchPrinciples.Add(new UserPrincipal(ctx) { DisplayName = String.Format("*{0}*", data.userid) });
                        searchPrinciples.Add(new UserPrincipal(ctx) { SamAccountName = String.Format("*{0}*", data.userid) });
                        searchPrinciples.Add(new UserPrincipal(ctx) { Surname = String.Format("*{0}*", data.userid) });
                        searchPrinciples.Add(new UserPrincipal(ctx) { GivenName = String.Format("*{0}*", data.userid) });

                        List<Principal> results = new List<Principal>();
                        var searcher = new PrincipalSearcher();

                        foreach (var item in searchPrinciples)
                        {
                            searcher = new PrincipalSearcher(item);
                            foreach (Principal found in searcher.FindAll())
                            {
                                DirectoryEntry directoryEntry = (DirectoryEntry)found.GetUnderlyingObject();
                                PropertyValueCollection fotoAD = directoryEntry.Properties["thumbnailPhoto"];
                                byte[] fotoEnBytes = new byte[0];

                                if (fotoAD.Value != null && fotoAD.Value is byte[])
                                {
                                    fotoEnBytes = (byte[])fotoAD.Value;
                                }
                                data.FotoBytes = fotoEnBytes;
                                data.foto = (fotoEnBytes.Length > 1) ? String.Format("data:image/jpeg;base64,{0}", Convert.ToBase64String(fotoEnBytes)) : null;
                                break;
                            }
                        }
                    }
                }
            }

            return anexos;
        }
    }
}
