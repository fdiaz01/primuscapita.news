﻿using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.News.Intranet.Configuracion
{
    public class IntranetCompletoNewsConfiguracion : EntityTypeConfiguration<IntranetCompletoNews>
    {
        public IntranetCompletoNewsConfiguracion()
        {
            this.HasKey(ip => ip.Id);

            this.ToTable("IntranetCompletoNews");

            this.HasMany(ip => ip.Post)
                .WithRequired(x => x.IntranetCompletoNewsAsociado)
                .WillCascadeOnDelete();
        }
    }
}
