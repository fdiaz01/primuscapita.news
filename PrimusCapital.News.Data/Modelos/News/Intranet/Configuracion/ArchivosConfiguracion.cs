﻿using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.News.Intranet.Configuracion
{
    public class ArchivosConfiguracion : EntityTypeConfiguration<Archivos>
    {
        public ArchivosConfiguracion()
        {
            this.HasKey(a => a.Id);

            this.ToTable("Archivos");
        }
    }
}
