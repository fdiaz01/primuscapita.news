﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.News.Intranet.Entidades
{
    public class Post
    {
        public int Id { get; set; }

        public string Comentarios { get; set; }
        public string Url { get; set; }
        public DateTime Fecha { get; set; }
        public IList<Archivos> FotosRelacionadas { get; set; }

        //public Post() { }

        public virtual IntranetCompletoNews IntranetCompletoNewsAsociado { get; set; }
    }
}
