﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.News.Intranet.Entidades
{
    public class Archivos
    {
        public int Id { get; set; }

        public string NombreArchivo { get; set; }
        public string NombreMostrar { get; set; }
        public string Descripccion { get; set; }
        public string Ruta { get; set; }
        public DateTime Fecha { get; set; }
        public string TipoArchivo { get; set; }
        public bool ArchivoPortada { get; set; }

        public virtual Post PostAsociado { get; set; }
    }
}
