﻿using PrimusCapital.News.Data.Modelos.News.Comun.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.News.Intranet.Entidades
{
    public class IntranetCompletoNews
    {
        public int Id { get; set; }

        public DateTime Fecha { get; set; }
        public string UsuarioPost { get; set; }

        //clases Para la informaciond e intranet
        public virtual IList<Post> Post { get; set; }
        public virtual IList<HistoricoIntranet> RegistroPostRealizadaAsociados { get; set; }

        public void Inicializar()
        {
            RegistroPostRealizadaAsociados = new List<HistoricoIntranet>();
            Post = new List<Post>();
        }
    }
}
