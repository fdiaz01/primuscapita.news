﻿using PrimusCapital.News.Data.Modelos.News.Comun.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.News.Comun.Configuracion
{
    public class ConfiguracionNewsConfiguracion : EntityTypeConfiguration<ConfiguracionNews>
    {
        public ConfiguracionNewsConfiguracion()
        {
            this.Ignore(x => x.DuracionCacheGlobal);
        }
    }
}
