﻿using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.News.Comun.Entidades
{
    public class HistoricoIntranet
    {
        public int Id { get; set; }

        public DateTime Fecha { get; set; }
        public string UusuarioPost { get; set; }
        public string Ip { get; set; }
        public bool CargadoDesdeCache { get; set; }

        public virtual IntranetCompletoNews IntranetCompletoNewsAsociado { get; set; }
    }
}
