﻿using PrimusCapital.News.Data.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.News.Init
{
    public class PrimusCacheDropAlways : DropCreateDatabaseAlways<PrimusCacheNewsDbContext>
    {
        protected override void Seed(PrimusCacheNewsDbContext context)
        {
            base.Seed(context);

            DatabaseSeeder.Seed(context);
        }
    }
}
