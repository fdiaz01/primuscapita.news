﻿using PrimusCapital.News.Data.Migrations;
using PrimusCapital.News.Data.Modelos.News.Comun.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.News.Init
{
    public static class DatabaseSeeder
    {
        public static void Seed(PrimusCacheNewsDbContext context)
        {
            // Configuraciones iniciales
            var config = new ConfiguracionNews()
            {
                DuracionCacheGlobal = TimeSpan.FromDays(1)
            };
            context.ConfiguracionNews.Add(config);
        }
    }
}
