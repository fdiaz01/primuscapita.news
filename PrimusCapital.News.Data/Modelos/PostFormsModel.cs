﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PrimusCapital.News.Data.Modelos
{
    [Serializable]
    public class PostFormsModel
    {
        public HttpRequestBase request { get; set; }
        public ErrorViewModel error { get; set; }

        public StringBuilder htmlPost { get; set; }
        public StringBuilder scriptFileInput { get; set; }
        public StringBuilder scriptSummernote { get; set; }
        public StringBuilder scriptValidacion { get; set; }

        public void clonarHtml(string ruta){
            string rutaCompleta = string.Empty, textoTemplate = string.Empty;
            rutaCompleta = ruta + "\\htmlPost.txt";
            if (File.Exists(rutaCompleta))
            {
                textoTemplate = File.ReadAllText(rutaCompleta);
            }
            int contador = 1;
            textoTemplate = textoTemplate.Replace("@@contador", contador.ToString());
            htmlPost = new StringBuilder();
            htmlPost.Append(textoTemplate);
            textoTemplate = null;
        }

        public void clonarScript(string ruta)
        {
            //fileinput
            string rutaCompleta= string.Empty, textoTemplate = string.Empty;
            rutaCompleta = ruta + "\\scriptFileinput.txt";
            if (File.Exists(rutaCompleta))
            {
                textoTemplate = File.ReadAllText(rutaCompleta);
            }
            int contador = 1;
            textoTemplate = textoTemplate.Replace("@@contador", contador.ToString());
            scriptFileInput = new StringBuilder();
            scriptFileInput.Append(textoTemplate);
            textoTemplate = null;

            //summernote
            textoTemplate = string.Empty;
            rutaCompleta = ruta + "\\scriptSummernote.txt";
            if (File.Exists(rutaCompleta))
            {
                textoTemplate = File.ReadAllText(rutaCompleta);
            }
            contador = 1;
            textoTemplate = textoTemplate.Replace("@@contador", contador.ToString());
            scriptSummernote = new StringBuilder();
            scriptSummernote.Append(textoTemplate);
            textoTemplate = null;

            //validacion
            textoTemplate = string.Empty;
            rutaCompleta = ruta + "\\scriptValidacion.txt";
            if (File.Exists(rutaCompleta))
            {
                textoTemplate = File.ReadAllText(rutaCompleta);
            }
            contador = 1;
            textoTemplate = textoTemplate.Replace("@@contador", contador.ToString());
            scriptValidacion = new StringBuilder();
            scriptValidacion.Append(textoTemplate);
            textoTemplate = null;
            rutaCompleta = null;
        }

        public void clonarHtml(string ruta, int cantidad)
        {
            htmlPost = new StringBuilder();
            string rutaCompleta = string.Empty, textoTemplate = string.Empty;
            for (int i = 1; i <= cantidad; i++)
            {
                rutaCompleta = ruta + "\\htmlPost.txt";
                if (File.Exists(rutaCompleta))
                {
                    textoTemplate = File.ReadAllText(rutaCompleta);
                }
                textoTemplate = textoTemplate.Replace("@@contador", i.ToString());
                htmlPost.Append(textoTemplate);
            }
            textoTemplate = null;
        }

        public void clonarScript(string ruta, int cantidad)
        {
            scriptFileInput = new StringBuilder();
            scriptSummernote = new StringBuilder();
            scriptValidacion = new StringBuilder();
            //fileinput
            string rutaCompleta = string.Empty, textoTemplate = string.Empty;
            for (int i = 1; i <= cantidad; i++)
            {
                rutaCompleta = ruta + "\\scriptFileinput.txt";
                if (File.Exists(rutaCompleta))
                {
                    textoTemplate = File.ReadAllText(rutaCompleta);
                }
                textoTemplate = textoTemplate.Replace("@@contador", i.ToString());
                scriptFileInput.Append(textoTemplate);
            }
            textoTemplate = null;

            //summernote
            textoTemplate = string.Empty;
            for (int i = 1; i <= cantidad; i++)
            {
                rutaCompleta = ruta + "\\scriptSummernote.txt";
                if (File.Exists(rutaCompleta))
                {
                    textoTemplate = File.ReadAllText(rutaCompleta);
                }
                textoTemplate = textoTemplate.Replace("@@contador", i.ToString());
                scriptSummernote.Append(textoTemplate);
            }
            textoTemplate = null;

            //validacion
            textoTemplate = string.Empty;
            for (int i = 1; i <= cantidad; i++)
            {
                rutaCompleta = ruta + "\\scriptValidacion.txt";
                if (File.Exists(rutaCompleta))
                {
                    textoTemplate = File.ReadAllText(rutaCompleta);
                }
                textoTemplate = textoTemplate.Replace("@@contador", i.ToString());
                scriptValidacion.Append(textoTemplate);
            }
            textoTemplate = null;
            rutaCompleta = null;
        }
    }
}
