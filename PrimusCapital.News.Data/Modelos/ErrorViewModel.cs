﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos
{
    [Serializable]
    public class ErrorViewModel
    {
        public int CodigoError { get; set; }
        public string DescripcionError { get; set; }

        public enum Importance
        {
            ErrornoClasificado,
            UsuarioVacio,
            UsuarioExpirada,
            DigitoClasificadorVacio,
            NroDocumentoVacio,
            DigitoVerificadorVacio,
            TipoDocumentoVacio
        }

        //Codigos Pre Definidos
        /*public ErrorViewModel CodigosPredefinidos()
        {

        }*/
    }
}
