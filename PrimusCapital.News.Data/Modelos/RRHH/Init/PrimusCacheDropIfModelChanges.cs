﻿using PrimusCapital.News.Data.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.RRHH.Init
{
    public class PrimusCacheDropIfModelChanges : DropCreateDatabaseIfModelChanges<PrimusCacheRRHHDbContext>
    {
        protected override void Seed(PrimusCacheRRHHDbContext context)
        {
            base.Seed(context);

            DatabaseSeeder.Seed(context);
        }
    }
}
