﻿using PrimusCapital.News.Data.Migrations;
using PrimusCapital.News.Data.Modelos.RRHH.Comun.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.RRHH.Init
{
    public static class DatabaseSeeder
    {
        public static void Seed(PrimusCacheRRHHDbContext context)
        {
            // Configuraciones iniciales
            var config = new ConfiguracionRRHH()
            {
                DuracionCacheGlobal = TimeSpan.FromDays(1)
            };
            context.ConfiguracionRRHH.Add(config);
        }
    }
}
