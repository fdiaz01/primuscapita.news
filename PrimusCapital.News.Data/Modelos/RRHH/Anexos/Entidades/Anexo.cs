﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.RRHH.Anexos.Entidades
{
    public class Anexo
    {
        public int id { get; set; }

        public string Apellido_P { get; set; }
        public string Apellido_M { get; set; }
        public string nombre { get; set; }
        public string fecha_n { get; set; }
        public string area { get; set; }
        public string anexo { get; set; }
        public string mail { get; set; }
        public string foto { get; set; }
        public string cargo { get; set; }
        public string userid { get; set; }
        public string IP { get; set; }
        public int Rut { get; set; }
        public string DV { get; set; }
        public string correosuperior { get; set; }
        public DateTime fec_fin_contrato { get; set; }
        public DateTime fec_ini_contrato { get; set; }
        public string vigente { get; set; }
        public string sucursal { get; set; }

        public Anexo() { }
    }
}
