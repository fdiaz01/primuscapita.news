﻿using PrimusCapital.News.Data.Modelos.RRHH.Anexos.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.RRHH.Anexos.Configuracion
{
    public class AnexoConfiguracion : EntityTypeConfiguration<Anexo>
    {
        public AnexoConfiguracion()
        {
            this.HasKey(a => a.id);

            this.ToTable("anexos");
        }
    }
}
