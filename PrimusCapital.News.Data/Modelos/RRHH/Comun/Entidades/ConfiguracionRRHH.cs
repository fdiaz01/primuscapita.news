﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.RRHH.Comun.Entidades
{
    public class ConfiguracionRRHH
    {
        public int Id { get; set; }
        public long DuracionCacheGlobalSegundos { get; set; }
        public TimeSpan DuracionCacheGlobal
        {
            get { return TimeSpan.FromSeconds(DuracionCacheGlobalSegundos); }
            set { DuracionCacheGlobalSegundos = (long)value.TotalSeconds; }
        }
    }
}
