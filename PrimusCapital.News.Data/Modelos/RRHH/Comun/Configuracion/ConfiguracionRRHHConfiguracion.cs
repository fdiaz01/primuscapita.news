﻿using PrimusCapital.News.Data.Modelos.RRHH.Comun.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos.RRHH.Comun.Configuracion
{
    public class ConfiguracionRRHHConfiguracion : EntityTypeConfiguration<ConfiguracionRRHH>
    {
        public ConfiguracionRRHHConfiguracion()
        {
            this.Ignore(x => x.DuracionCacheGlobal);
        }
    }
}
