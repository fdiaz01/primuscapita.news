﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos
{
    public class UsuarioAD
    {
        public string Username { get; set; }
        public int Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public bool Logueado { get; set; }
        public bool Permitido { get; set; }
        public byte[] FotoBytes { get; set; }
        public string FotoUrl { get; set; }
        public List<string> Roles { get; set; }
        public string NombreCompleto { get { return string.Format("{0} {1}", this.Nombre, this.Apellido); } }
        public string MensajeDetalleError { get; set; }

        public UsuarioAD()
        {
            Logueado = false;
            Roles = new List<string>();
        }
    }
}
