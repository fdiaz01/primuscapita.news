﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimusCapital.News.Data.Modelos
{
    [Serializable]
    public class IntranetCompletoNewsModel
    {
        public UsuarioAD Usuario { get; set; }
        public List<RegistrosIntranet> Intranet { get; set; }
        public List<AnexosModel> Anexos { get; set; }
        public ErrorViewModel error { get; set; }

        public IntranetCompletoNewsModel()
        {
            Usuario = new UsuarioAD();
            Intranet = new List<RegistrosIntranet>();
            error = new ErrorViewModel();
        }

        public class RegistrosIntranet
        {
            public int Id { get; set; }

            public DateTime Fecha { get; set; }
            public string UsuarioPost { get; set; }

            //clases Para la informaciond e intranet
            public List<PostModel> Posts { get; set; }

            public RegistrosIntranet()
            {
                Posts = new List<PostModel>();
            }

            public class PostModel
            {
                public int Id { get; set; }

                public string Comentarios { get; set; }
                public string Url { get; set; }
                public DateTime Fecha { get; set; }
                public List<ArchivosModel> FotosRelacionadas { get; set; }

                public PostModel()
                {
                    FotosRelacionadas = new List<ArchivosModel>();
                }
            }

            public class ArchivosModel
            {
                public int Id { get; set; }

                public string NombreArchivo { get; set; }
                public string NombreMostrar { get; set; }
                public string Descripccion { get; set; }
                public string Ruta { get; set; }
                public DateTime Fecha { get; set; }
                public string TipoArchivo { get; set; }
                public bool ArchivoPortada { get; set; }

                public ArchivosModel() { }
            }
        }

        public class AnexosModel
        {
            public int id { get; set; }

            public string Apellido_P { get; set; }
            public string Apellido_M { get; set; }
            public string nombre { get; set; }
            public string fecha_n { get; set; }
            public DateTime fecha_nacimiento { get; set; }
            public string area { get; set; }
            public string anexo { get; set; }
            public string mail { get; set; }
            public string foto { get; set; }
            public string cargo { get; set; }
            public string userid { get; set; }
            public string IP { get; set; }
            public int Rut { get; set; }
            public string DV { get; set; }
            public string correosuperior { get; set; }
            public DateTime fec_fin_contrato { get; set; }
            public DateTime fec_ini_contrato { get; set; }
            public string vigente { get; set; }
            public string sucursal { get; set; }
            public byte[] FotoBytes { get; set; }

            public AnexosModel() { }
        }
    }
}
