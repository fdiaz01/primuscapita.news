﻿using PrimusCapital.News.Data.Modelos.News.Comun.Configuracion;
using PrimusCapital.News.Data.Modelos.News.Comun.Entidades;
using PrimusCapital.News.Data.Modelos.News.Init;
using PrimusCapital.News.Data.Modelos.News.Intranet.Configuracion;
using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace PrimusCapital.News.Data.Migrations
{
    public class PrimusCacheNewsDbContext : DbContext
    {
        static string Ambiente = WebConfigurationManager.AppSettings["TipoAmbiente"].ToString();
        public DbSet<IntranetCompletoNews> RegistroIntranet { get; set; }
        public DbSet<ConfiguracionNews> ConfiguracionNews { get; set; }
        public DbSet<HistoricoIntranet> RegistroHistorico { get; set; }
        public DbSet<Post> Post { get; set; }
        public DbSet<Archivos> Archivos { get; set; }

        static PrimusCacheNewsDbContext()
        {
            #if DEBUG
            Database.SetInitializer(new PrimusCacheDropIfModelChanges());
            #endif
        }

        public PrimusCacheNewsDbContext()
            : base(WebConfigurationManager.ConnectionStrings[Ambiente + "BD_News"].ToString())
        { }

        public PrimusCacheNewsDbContext(string connectionString)
            : base(connectionString)
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Remover pluralizacion automatica de tablas al estilo ingles
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // Config. comunes
            modelBuilder.Configurations.Add(new ConfiguracionNewsConfiguracion());
            modelBuilder.Configurations.Add(new HistoricoIntranetConfiguracion());

            // Configuraciones
            modelBuilder.Configurations.Add(new IntranetCompletoNewsConfiguracion());
            modelBuilder.Configurations.Add(new PostConfiguracion());
            modelBuilder.Configurations.Add(new ArchivosConfiguracion());
        }
    }
}
