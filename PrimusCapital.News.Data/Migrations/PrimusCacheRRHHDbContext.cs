﻿using PrimusCapital.News.Data.Modelos.RRHH.Init;
using PrimusCapital.News.Data.Modelos.RRHH.Anexos.Configuracion;
using PrimusCapital.News.Data.Modelos.RRHH.Anexos.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using PrimusCapital.News.Data.Modelos.RRHH.Comun.Entidades;

namespace PrimusCapital.News.Data.Migrations
{
    public class PrimusCacheRRHHDbContext : DbContext
    {
        static string Ambiente = WebConfigurationManager.AppSettings["TipoAmbiente"].ToString();
        public DbSet<Anexo> RegistroAnexo { get; set; }
        public DbSet<ConfiguracionRRHH> ConfiguracionRRHH { get; set; }

        static PrimusCacheRRHHDbContext()
        {
            #if DEBUG
            Database.SetInitializer(new PrimusCacheDropIfModelChanges());
            #endif
        }

        public PrimusCacheRRHHDbContext()
            : base(WebConfigurationManager.ConnectionStrings[Ambiente + "BD_RR_HH"].ToString())
        { }

        public PrimusCacheRRHHDbContext(string connectionString)
            : base(connectionString)
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Configuraciones Informe Sentinel
            modelBuilder.Configurations.Add(new AnexoConfiguracion());
        }
    }
}
