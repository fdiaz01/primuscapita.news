namespace PrimusCapital.News.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IntranetV010 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConfiguracionNews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DuracionCacheGlobalSegundos = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HistoricoIntranet",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        UusuarioPost = c.String(),
                        Ip = c.String(),
                        CargadoDesdeCache = c.Boolean(nullable: false),
                        IntranetCompletoNewsAsociado_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IntranetCompletoNews", t => t.IntranetCompletoNewsAsociado_Id, cascadeDelete: true)
                .Index(t => t.IntranetCompletoNewsAsociado_Id);
            
            CreateTable(
                "dbo.IntranetCompletoNews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        UsuarioPost = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Post",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Comentarios = c.String(),
                        Url = c.String(),
                        Fecha = c.DateTime(nullable: false),
                        IntranetCompletoNewsAsociado_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IntranetCompletoNews", t => t.IntranetCompletoNewsAsociado_Id, cascadeDelete: true)
                .Index(t => t.IntranetCompletoNewsAsociado_Id);
            
            CreateTable(
                "dbo.Archivos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreArchivo = c.String(),
                        NombreMostrar = c.String(),
                        Descripccion = c.String(),
                        Ruta = c.String(),
                        Fecha = c.DateTime(nullable: false),
                        TipoArchivo = c.String(),
                        ArchivoPortada = c.Boolean(nullable: false),
                        PostAsociado_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Post", t => t.PostAsociado_Id, cascadeDelete: true)
                .Index(t => t.PostAsociado_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HistoricoIntranet", "IntranetCompletoNewsAsociado_Id", "dbo.IntranetCompletoNews");
            DropForeignKey("dbo.Post", "IntranetCompletoNewsAsociado_Id", "dbo.IntranetCompletoNews");
            DropForeignKey("dbo.Archivos", "PostAsociado_Id", "dbo.Post");
            DropIndex("dbo.Archivos", new[] { "PostAsociado_Id" });
            DropIndex("dbo.Post", new[] { "IntranetCompletoNewsAsociado_Id" });
            DropIndex("dbo.HistoricoIntranet", new[] { "IntranetCompletoNewsAsociado_Id" });
            DropTable("dbo.Archivos");
            DropTable("dbo.Post");
            DropTable("dbo.IntranetCompletoNews");
            DropTable("dbo.HistoricoIntranet");
            DropTable("dbo.ConfiguracionNews");
        }
    }
}
