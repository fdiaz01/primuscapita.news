﻿using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using PrimusCapital.News.Aplicacion.Componentes;
using PrimusCapital.News.Aplicacion.Interfaces;
using PrimusCapital.News.Data.Migrations;
using PrimusCapital.News.WebUI.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WebActivatorEx;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof(NinjectWebCommon), "Stop")]

namespace PrimusCapital.News.WebUI.App_Start
{
    public class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch (Exception e)
            {
                GestorLog.GeneraLog(e);
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            string Ambiente = WebConfigurationManager.AppSettings["TipoAmbiente"].ToString();
            string connectionString = WebConfigurationManager.ConnectionStrings[Ambiente + "BD_News"].ConnectionString;
            string connectionStringRRHH = WebConfigurationManager.ConnectionStrings[Ambiente + "BD_RR_HH"].ConnectionString;

            kernel.Bind<PrimusCacheNewsDbContext>().ToSelf()
                  .InRequestScope()
                  .WithConstructorArgument("connectionString", connectionString);

            kernel.Bind<PrimusCacheRRHHDbContext>().ToSelf()
                  .InRequestScope()
                  .WithConstructorArgument("connectionString", connectionStringRRHH);

            kernel.Bind<INewsCacheGateway>().To<NewsCacheGateway>();
            kernel.Bind<IAppService>().To<AppService>();
        }
    }
}