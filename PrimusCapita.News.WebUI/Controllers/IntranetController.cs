﻿using PrimusCapital.News.Aplicacion.Componentes;
using PrimusCapital.News.Aplicacion.Interfaces;
using PrimusCapital.News.Data.Modelos;
using PrimusCapital.News.Data.Modelos.News.Intranet.Entidades;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace PrimusCapital.News.WebUI.Controllers
{
    public class IntranetController : Controller
    {
        private readonly IAppService _appService;

        public IntranetController(IAppService appService)
        {
            _appService = appService;
        }

        public ActionResult Index()
        {
            ErrorViewModel errorModel = new ErrorViewModel();
            IntranetCompletoNewsModel model = null;
            UsuarioAD user = null;

            try
            {
                if (Session["infoUsuario"] != null)
                {
                    user = new UsuarioAD();
                    user = Session["infoUsuario"] as UsuarioAD;
                }
                 
                if (TempData["errorModel"] != null)
                {
                    errorModel = TempData["errorModel"] as ErrorViewModel;
                    model = null;
                }

                model = new IntranetCompletoNewsModel();
                model = _appService.ObtenerPosts();
                model.error = errorModel;

                //anexos
                BDaccess BDaccess = new BDaccess();
                List<IntranetCompletoNewsModel.AnexosModel> listaAnexos = new List<IntranetCompletoNewsModel.AnexosModel>();
                listaAnexos = BDaccess.ListaAnexos();

                if (listaAnexos != null)
                {
                    model.Anexos = listaAnexos;
                    //model.Anexos = _appService.ObtenerFotosAnexos(listaAnexos);
                }
                    
                if (user != null)
                    model.Usuario = user;

                return View("~/Views/Intranet/Index.cshtml", model);
                //return View();
            }
            catch (Exception e)
            {
                GestorLog.GeneraLog(e);

                errorModel = new ErrorViewModel();
                errorModel.CodigoError = 0;
                errorModel.DescripcionError = "Error no Clasificado";
                return View("~/Views/Intranet/Error.cshtml", errorModel);
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult AgregarPost(FormCollection form)
        {
            //dynamic showMessageString = string.Empty;  
            ErrorViewModel errorModel = new ErrorViewModel();
            //IntranetCompletoNewsModel model = null;

            try
            {
                if (Session["infoUsuario"] == null)
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Session expirada.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                StringBuilder comentarios = new StringBuilder();
                if (form["summernote"] != null)
                {
                    comentarios.Append(form["summernote"].Trim());
                }
                else
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Error no existen comentarios.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }
                
                if (Request.Files.Count <= 0)
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Error no existen archivos.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                string url = string.Empty;
                if (form["basic_url"] != null)
                {
                    url = form["basic_url"].Trim();
                }
                /*else
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Error no existen URL referencia.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }*/

                //hacer que devuelva el error.
                IntranetCompletoNews intranet = null;
                string root = Server.MapPath("~");
                //string parent = Path.GetDirectoryName(root);
                //string grandParent = Path.GetDirectoryName(parent);

                string Ambiente = WebConfigurationManager.AppSettings["TipoAmbiente"].ToString();
                string RutaArchivosImagenes = WebConfigurationManager.AppSettings[Ambiente + "RutaArchivosImagenes"].ToString();

                string usuario = string.Empty;
                if (Session["infoUsuario"] != null)
                {
                    usuario = (Session["infoUsuario"] as UsuarioAD).Username;
                }
                string ipUser = Request.UserHostAddress;

                if (Request.Files.Count > 0)
                    intranet = _appService.GuardarPost(0, comentarios, Request.Files, url, root + RutaArchivosImagenes, usuario, ipUser, out errorModel);

                //model = new IntranetCompletoNewsModel();
                //model = _appService.ObtenerPosts();

                if (errorModel.CodigoError >= 0)
                {
                    return RedirectToAction("Index", "Intranet");
                }
                else
                {
                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }
            }
            catch (Exception e)
            {
                GestorLog.GeneraLog(e);

                errorModel = new ErrorViewModel();
                errorModel.CodigoError = 0; //-1
                errorModel.DescripcionError = "Error no Clasificado"; //e.ToString();
                return View("~/Views/Intranet/Error.cshtml", errorModel);
            } 
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Acceso(FormCollection form)
        { 
            ErrorViewModel errorModel = new ErrorViewModel();

            try
            {
                string username = string.Empty;
                if (form["username"] != null)
                {
                    username = form["username"].Trim();
                }
                else
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Usuario o contraseña Incorrectos!";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                string password = string.Empty;
                if (form["password"] != null)
                {
                    password = form["password"].Trim();
                }
                else
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Usuario o contraseña Incorrectos!";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                var user = _appService.UsuarioValido(username, password, out errorModel);
                
                if (errorModel.CodigoError >= 0){
                    Session["infoUsuario"] = user;
                    return RedirectToAction("Index", "Intranet");
                } 
                else
                {
                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }
            }
            catch (Exception e)
            {
                GestorLog.GeneraLog(e);

                errorModel = new ErrorViewModel();
                errorModel.CodigoError = 0;
                errorModel.DescripcionError = "Error no Clasificado";
                return View("~/Views/Intranet/Error.cshtml", errorModel);
            }
        }

        public ActionResult Salir()
        {
            Session["infoUsuario"] = null;
            Session.Abandon();
            return RedirectToAction("Index", "Intranet");
        }

        [HttpPost]
        public ActionResult EliminarPost(FormCollection form)
        {
            ErrorViewModel errorModel = new ErrorViewModel();

            try
            {
                if (Session["infoUsuario"] == null)
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Session expirada.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                int IdPost = 0;
                if (form["idPostE"] != null)
                {
                    IdPost = int.Parse(form["idPostE"].Trim());
                }
                else
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Error al tratar de eliminar el Post.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                if (IdPost <= 0)
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Error al tratar de eliminar el Post.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                string usuario = string.Empty;
                if (Session["infoUsuario"] != null)
                {
                    usuario = (Session["infoUsuario"] as UsuarioAD).Username;
                }
                string ipUser = Request.UserHostAddress;

                _appService.EliminarPost(IdPost, usuario, ipUser, out errorModel);

                if (errorModel.CodigoError >= 0)
                {
                    return RedirectToAction("Index", "Intranet");
                }
                else
                {
                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }
            }
            catch (Exception e)
            {
                GestorLog.GeneraLog(e);

                errorModel = new ErrorViewModel();
                errorModel.CodigoError = 0;
                errorModel.DescripcionError = "Error no Clasificado";
                return View("~/Views/Intranet/Error.cshtml", errorModel);
            }
        }
    
        [HttpPost]
        public JsonResult ObtenerPost(int IdPost)
        {
            ErrorViewModel errorModel = new ErrorViewModel();
            IntranetCompletoNewsModel.RegistrosIntranet.PostModel model = null;
            UsuarioAD user = null;

            try
            {
                if (Session["infoUsuario"] != null)
                {
                    user = new UsuarioAD();
                    user = Session["infoUsuario"] as UsuarioAD;
                }

                if (TempData["errorModel"] != null)
                {
                    errorModel = TempData["errorModel"] as ErrorViewModel;
                    return Json(errorModel, JsonRequestBehavior.AllowGet);
                }

                model = new IntranetCompletoNewsModel.RegistrosIntranet.PostModel();
                model = _appService.ObtenerPostSegunId(IdPost);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                GestorLog.GeneraLog(e);

                errorModel = new ErrorViewModel();
                errorModel.CodigoError = 0;
                errorModel.DescripcionError = "Error no Clasificado";

                return Json(errorModel, JsonRequestBehavior.AllowGet);
                //return View("~/Views/Intranet/Error.cshtml", errorModel);
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EditarPost(FormCollection form)
        {
            ErrorViewModel errorModel = new ErrorViewModel();

            try
            {
                if (Session["infoUsuario"] == null)
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Session expirada.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                StringBuilder comentarios = new StringBuilder();
                if (form["summernote_edit"] != null)
                {
                    comentarios.Append(form["summernote_edit"].Trim());
                }
                else
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Error no existen comentarios.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                if (Request.Files.Count <= 0)
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Error no existen archivos.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                string url = string.Empty;
                if (form["basic_url_edit"] != null)
                {
                    url = form["basic_url_edit"].Trim();
                }
                /*else
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Error no existen URL referencia.";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }*/


                int idPost = 0;
                if (form["idPostEditar"] != null)
                {
                    int valorInt = 0;
                    int.TryParse(form["idPostEditar"].Trim(), NumberStyles.Any, CultureInfo.InvariantCulture, out valorInt);
                    idPost = valorInt;

                    if (idPost <= 0)
                    {
                        errorModel.CodigoError = -1;
                        errorModel.DescripcionError = "Error no al eliminar el Post";

                        TempData["errorModel"] = errorModel;
                        return RedirectToAction("Index", "Intranet");
                    }
                }
                else
                {
                    errorModel.CodigoError = -1;
                    errorModel.DescripcionError = "Error no al eliminar el Post";

                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }

                //asumimos que si no trae nada no se modifico las fotos
                string IdsArchivosBorrar = string.Empty;
                List<int> IdsArchivos = new List<int>();
                if (form["IdsArchivosBorrar"] != null)
                {
                    IdsArchivosBorrar = form["IdsArchivosBorrar"].Trim();
                    if (!string.IsNullOrEmpty(IdsArchivosBorrar))
                    {
                        string[] Ids = IdsArchivosBorrar.Split(',');

                        foreach (var data in Ids)
                        {
                            int valorInt = 0;
                            int.TryParse(data.Trim(), NumberStyles.Any, CultureInfo.InvariantCulture, out valorInt);
                            if (valorInt > 0)
                                IdsArchivos.Add(valorInt);
                        }
                    }
                }

                /*int CantidadOriginalArchivos = 0;
                if (form["CantidadArchivosCargadosAnterior"] != null)
                {
                    int valorInt = 0;
                    int.TryParse(form["CantidadArchivosCargadosAnterior"], NumberStyles.Any, CultureInfo.InvariantCulture, out valorInt);
                    CantidadOriginalArchivos = valorInt;

                    if (CantidadOriginalArchivos >= 0)
                    {
                        if (form["IdsArchivosBorrar"] != null)
                        {
                            IdsArchivos = form["IdsArchivosBorrar"].Trim();
                        }
                    }
                }*/

                IntranetCompletoNews intranet = null;
                string root = Server.MapPath("~");
                //string parent = Path.GetDirectoryName(root);
                //string grandParent = Path.GetDirectoryName(parent);

                string Ambiente = WebConfigurationManager.AppSettings["TipoAmbiente"].ToString();
                string RutaArchivosImagenes = WebConfigurationManager.AppSettings[Ambiente + "RutaArchivosImagenes"].ToString();

                string usuario = string.Empty;
                if (Session["infoUsuario"] != null)
                {
                    usuario = (Session["infoUsuario"] as UsuarioAD).Username;
                }
                string ipUser = Request.UserHostAddress;

                if (Request.Files.Count > 0)
                    intranet = _appService.EditarPost(idPost, IdsArchivos, comentarios, Request.Files, url, root + RutaArchivosImagenes, usuario, ipUser, out errorModel);

                if (errorModel.CodigoError >= 0)
                {
                    return RedirectToAction("Index", "Intranet");
                }
                else
                {
                    TempData["errorModel"] = errorModel;
                    return RedirectToAction("Index", "Intranet");
                }
            }
            catch (Exception e)
            {
                GestorLog.GeneraLog(e);

                errorModel = new ErrorViewModel();
                errorModel.CodigoError = 0; //-1
                errorModel.DescripcionError = "Error no Clasificado"; //e.ToString();
                return View("~/Views/Intranet/Error.cshtml", errorModel);
            }
        }
    }
}